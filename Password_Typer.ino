#include <ADCTouchSensor.h>

#ifdef ADCTOUCH_INTERNAL_GROUNDING
# define GROUNDED_PIN -1
#endif

#if defined(ARDUINO_ARCH_AVR)
# define PIN1 A0
#else
# define PIN1 PA0
# ifndef ADCTOUCH_INTERNAL_GROUNDING
#  define GROUNDED_PIN PA3
# endif
#endif

ADCTouchSensor button0 = ADCTouchSensor(PIN1, GROUNDED_PIN); 

#include "Keyboard.h"


void setup() {
  button0.begin();
  
  Keyboard.begin();
}

void loop() {
  // read the pushbutton:
    int value0 = button0.read();
    if (value0 > 40) {
  
    // type out a message
    Keyboard.print("USERNAME");
    delay(100);
    Keyboard.press(KEY_TAB);
    delay(50);
    Keyboard.release(KEY_TAB);
    Keyboard.print("PASSWORD");
    delay(100);
    Keyboard.press(KEY_RETURN);
    delay(50);
    Keyboard.release(KEY_RETURN);
    delay(500);
    }
 }