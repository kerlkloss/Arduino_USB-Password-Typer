# Arduino_USB-Password-Typer

A project basen on Arduini Leonardo / ATMega32U4, that behaves as a USB keyboard, and automatically types in your credentials when you touch the PCB.

Uses one ADC pin as a touch input. 

Works with the bare board, no need to add a touch pad or button.

Best to use with a USB stick containing an ATMega32U4.

![alt text](32u4.jpg)